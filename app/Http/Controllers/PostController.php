<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index()
    {
        return Post::all();
    }

    public function show($id)
    {
        $post = Post::where('id',$id)->firstOrFail();

        if(!$post)
        {
           return redirect('/')->withErrors('requested page not found');
        }

        return $post;
    }

    public function showByUser(Request $request, $userId = null)
    {
        if($userId == null){
            $userId =  $request->user()->id;
        }

        $comments = Post::where('author_id',$userId)->firstOrFail();
        return $comments;
    }

    public function store(Request $request)
    {
        $post = new Post();
        $post->title = $request->get('title');
        $post->content = $request->get('content');
        $post->slug = str_slug($request->get('title'));
        $post->author_id = $request->user()->id;
        $post->active = 1;
        $post->save();

        return response()->json($post, 201);
    }

    public function update(Request $request, $id)
    {
        $post = Post::where('id',$id)->first();
        $post->title = $request->get('title');
        $post->content = $request->get('content');
        $post->active = 1;
        $post->save();

        return response()->json($post, 200);
    }

    public function delete($id)
    {
        $post = Post::where('id',$id)->first();
        $post->delete();

        return response()->json(null, 204);
    }
}
