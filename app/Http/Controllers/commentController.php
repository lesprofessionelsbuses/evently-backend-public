<?php

namespace App\Http\Controllers;

use App\Posts;
use App\Comments;
use Redirect;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Comment;


class CommentController extends Controller {
    public function store(Request $request)
    {
        $comment = new Comment();
        $comment->from_user = $request->user()->id;
        $comment->on_post = $request->input('on_post');
        $comment->content = $request->input('content');
        $comment->save();

        return response()->json($comment, 201);
    }

    public function show($postId){
        $comments = Comment::where('on_post',$postId)->firstOrFail();
        return $comments;
    }

    public function showByUser(Request $request, $userId = null)
    {
        if($userId == null){
            $userId =  $request->user()->id;
        }

        $comments = Comment::where('from_user',$userId)->firstOrFail();
        return $comments;
    }

    public function update(Request $request, $id)
    {
        $comment = Comment::where('id',$id)->first();
        $comment->content = $request->get('content');
        $comment->save();

        return response()->json($comment, 200);
    }

    public function delete($id)
    {
        $post = Comment::where('id',$id)->first();
        $post->delete();

        return response()->json(null, 204);
    }



}