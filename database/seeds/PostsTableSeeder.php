<?php

use Illuminate\Database\Seeder;
use \App\Post;

class PostsTableSeeder extends Seeder
{

    public function run()
    {
        \DB::table('posts')->delete();
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        Post::truncate();
        $faker = \Faker\Factory::create();
        for ($i = 0; $i < 50; $i++) {
            Post::create([
                'author_id' => rand(0,19),
                'title' => $faker->sentence,
                'content' => $faker->paragraph,
                'slug' => str_slug($faker->sentence),
                'active' => 1
            ]);
        }
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');

    }
}