<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->delete();
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        User::truncate();
        $faker = \Faker\Factory::create();

        User::create([
            'id' => 1,
            'name' => 'admin',
            'email' => 'admin@admin.dev',
            'password' => Hash::make('password'),
            'role' => 'admin'
        ]);

        for ($i = 0; $i < 10; $i++) {
            User::create([
                'name' => $faker->firstname,
                'email'=> $faker->email,
                'password' => Hash::make('password'),
                'role' => 'author'
            ]);
        }
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}