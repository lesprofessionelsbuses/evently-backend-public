<?php

use Illuminate\Database\Seeder;
use \App\Comment;

class CommentsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        \DB::table('comments')->delete();
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        Comment::truncate();
        $faker = \Faker\Factory::create();
        for ($i = 0; $i < 150; $i++) {
            Comment::create([
                'on_post' => rand(0,50),
                'from_user' => rand(0,9),
                'content' => $faker->paragraph
            ]);
        }
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');


    }
}