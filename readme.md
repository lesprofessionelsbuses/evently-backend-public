#Laravel User Manager API

User manager with Laravel 5. Connect via simple token.

#Setup
* `composer install`
* create a mysql database
* copy .env.example to .env and adapt to your configuration
* `composer require laravel/passport`
* add `Laravel\Passport\PassportServiceProvider::class,` in /config/app.php file (proviers array)
* `php artisan config:cache`
* `composer dump-autoload`
* `php artisan migrate --seed`
* `php artisan passport:install` to create keys for Oauth