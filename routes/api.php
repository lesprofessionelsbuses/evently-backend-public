<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'Auth\RegisterController@register');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout');

Route::get('user/{id}/comments/', 'CommentController@showByUser'); // Get user's comments
Route::get('user/{id}/posts/', 'PostController@showByUser'); // Get user's posts

Route::group(['middleware' => 'auth:api'], function() {
    Route::get('post', 'PostController@index'); // Get all posts
    Route::get('post/{id}', 'PostController@show'); // Get post by id
    Route::post('post','PostController@store'); // Create new post
    Route::put('post/{id}','PostController@update'); // Update post by id
    Route::delete('post/{id}', 'PostController@delete'); // Delete post by id

    Route::get('comment', 'CommentController@index'); // Get all comments (why ? dunno)
    Route::get('comment/{id}', 'CommentController@show'); // Get comment by id
    Route::post('comment', 'CommentController@store'); // Create comment
    Route::put('comment/{id}', 'CommentController@update'); // Update comment
    Route::delete('comment/{id}', 'CommentController@delete'); // Delete comment

    Route::get('user', 'UserController@show'); // Get connected user's infos
    Route::get('user/comments/', 'CommentController@showByUser'); // Get connected user's comments
    Route::get('user/posts/', 'PostController@showByUser'); // Get connected user's posts

});